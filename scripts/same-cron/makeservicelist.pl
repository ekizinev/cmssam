#!/usr/bin/perl
#
# Script to modify the SAM configuration to have an explicit list of SRM instances to test
#
# Usage: makesrmlist.pl sam_conf new_sam_conf serviceabbr vo
#

use File::Temp qw/tempfile tmpnam/;
use File::Copy qw/copy/;

$same = $ENV{"SAME_HOME"};
$sameconf = $ARGV[0];
$outconf = $ARGV[1];
$servicetype = $ARGV[2];
$vo = $ARGV[3];

# Read list of bad services
$badservices = 'https://cern.ch/asciaba/SitesList/Blacklist.txt';
$ret = system("wget $badservices -O /tmp/badservices");
if ( $ret ) {
  warn "Error downloading bad service lists\n";
} else {
  open(FILE, "cat /tmp/badservices | dos2unix |") or warn "Cannot read temporary file\n";
  while (<FILE>) {
    chomp;
    if ( /#\s*${servicetype}\s*$/ ) {
      $read = 1;
      next;
    }
    if ( $read && /^\#/ ) {
      $read = 0;
      next;
    }
    next if ( $read == 0 );
    next unless /\w+/;
    push @bad, $_;
  }
  close FILE;
  unlink('/tmp/badservices');
}

# Parse the original configuration file and build filter to get the nodenames
open(CONF, $sameconf) or die "Cannot open $sameconf\n";
while (<CONF>) {
  chomp;
  if (/^common_filter=\"(.*)\"/) {
    $filter = $1;
  } elsif (/^${servicetype}_filter=\"(.*)\"/) {
    $filter .= " $1";
  }
}
close CONF;
$filter =~ s/%\(master_vo\)s/$vo/;
$temp = tmpnam() or die "Cannot create a temporary filename\n";
$cmd = "$same/bin/same-query nodename $filter > $temp";
$ret = system("$cmd");
if ( $ret ) {
  unlink $temp;
  warn "Query to SAM database failed, defaulting to original conf file\n";
  copy $sameconf, $outconf;
  exit 0;
}
$newfilter = "common_filter=\"nodename=";
open(LIST, "$temp") or die "Cannot open $temp\n";
while (<LIST>) {
  $node = $_;
  chomp $node;
  $newfilter .= "$node," if (!grep($node eq $_, @bad));
}
close LIST;
unlink $temp;
$newfilter =~ s/,$/\"/;
open(CONF, "$sameconf") or die "Cannot open $sameconf\n";
open(NEWCONF, "> $outconf") or die "Cannot open $outconf\n";
while (<CONF>) {
  chomp;
  if ( /^common_filter/ ) {
    $_ = $newfilter;
  }
  print NEWCONF "$_\n";
}
close NEWCONF;
close CONF;

