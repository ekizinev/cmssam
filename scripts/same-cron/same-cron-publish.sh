#!/bin/bash --login

# Define the Myproxy server to use
myproxy=myproxy-fts.cern.ch

# Define location of the cron jobs
SAME_CRON=$HOME/same-cron

# Define location of proxy
TMPFILE=`mktemp`
if [ $? -ne 0 ] ; then
  echo "Failed to create temporary file. Exiting..."
  exit 1
fi
export X509_USER_PROXY=$TMPFILE

# START of block to duplicate to add new SAM instances

# Define the location of the log file
log=$SAME_CRON/same-cron-publish.log

#Define the location of SAM
SAME_DIR=$HOME/same

# Define which sensors to run and to publish
publish_sensors="CE"

echo "" >> $log
echo "" >> $log
echo -n "----------------------[ " >> $log
date >> $log

# Define the location of your certificate
export X509_USER_CERT=$HOME/.globus-asciaba/usercert.pem
export X509_USER_KEY=$HOME/.globus-asciaba/userkey.pem

# Choose an FQAN and a lifetime for the VOMS extensions
fqan='/cms/Role=lcgadmin'
lifetime='24:00'

echo "+++ Using the $fqan FQAN" >> $log

voms-proxy-destroy
voms-proxy-init -debug -valid $lifetime -voms cms:$fqan -pwstdin < $HOME/.grid-cert-passphrase >> $log 2>&1
if [ $? != 0 ] ; then
  echo "Proxy creation failed!" >> $log
  publish_sensors=""
fi

for sensor in $publish_sensors ; do
echo "" >> $log	
echo "----------" >> $log
echo "Publishing $sensor sensor: " >> $log
echo "" >> $log
$SAME_DIR/client/bin/same-exec --publish $sensor >> $log 2>&1
done

# end of block to duplicate to add new SAM instances
# START of block to duplicate to add new SAM instances

# Define the location of the log file
log=$SAME_CRON/same-prod-cron-publish.log

#Define the location of SAM
SAME_DIR=$HOME/same-prod

# Define which sensors to run and to publish
publish_sensors="CE"

echo "" >> $log
echo "" >> $log
echo -n "----------------------[ " >> $log
date >> $log

# Define the location of your certificate
export X509_USER_CERT=$HOME/.globus-newca/usercert.pem
export X509_USER_KEY=$HOME/.globus-newca/userkey.pem

# Choose an FQAN and a lifetime for the VOMS extensions
fqan='/cms/Role=production'
lifetime='48:00'

echo "+++ Using the $fqan FQAN" >> $log

voms-proxy-destroy
voms-proxy-init -debug -valid $lifetime -voms cms:$fqan -pwstdin < $HOME/.grid-cert-passphrase >> $log 2>&1
if [ $? != 0 ] ; then
  echo "Proxy creation failed!" >> $log
  publish_sensors=""
fi

for sensor in $publish_sensors ; do
echo "" >> $log
echo "----------" >> $log
echo "Publishing $sensor sensor: " >> $log
echo "" >> $log
$SAME_DIR/client/bin/same-exec --publish $sensor >> $log 2>&1
done

# end of block to duplicate to add new SAM instances
