#!/bin/bash --login

# Source UI environment
source `dirname $0`/ui.sh
if [ $? -ne 0 ] ; then
  /bin/echo "--- FATAL: error in sourcing the environment"
  exit 1
fi

# Define location of the cron jobs
SAME_CRON=$HOME/same-cron

# Check for lock file
if [ -e $SAME_CRON/lock-ce-prod ] ; then
  if [ $((`date +%s` - `stat -c %Y $SAME_CRON/lock-ce-prod`)) -ge 21600 ] ; then
    echo "--- Sensor execution skipped, lock file present and older than 6 hours"
  fi
  exit 0
fi

# Create lock file
touch $SAME_CRON/lock-ce-prod
if [ $? -ne 0 ] ; then
  /bin/echo "--- FATAL: could not create lock file"
  exit 1
fi

# Define location of proxy
TMPFILE=`mktemp`
if [ $? -ne 0 ] ; then
  /bin/rm -f $SAME_CRON/lock-ce-prod
  /bin/echo "--- FATAL: could not create temp file"
  exit 1
fi

#Define the location of SAM
SAME_DIR=$HOME/same-prod

#Define working directory for this SAM instance. The actual directory will be $HOME/.$SAME_WORKDIR
SAME_WORKDIR=`basename $SAME_DIR`

# Clean up old work files
/usr/bin/find $HOME/.same-prod/*/nodes/* -maxdepth 0 -mtime +2 -ls -exec /bin/rm -rf {} \;

# Define which sensors to run and to publish
publish_sensors="CE CREAMCE"
sensors="CE CREAMCE"

echo -n "STARTTIME: "
date "+%Y-%m-%d %H:%M:%S"

# Update CMS scripts from CVS (comment if you do not want automatic updates)
$SAME_CRON/cms2sam.sh $SAME_DIR 2>&1
if [ $? -ne 0 ] ; then
  /bin/echo "--- FATAL: could not update CMS files"
  exit 1
fi

# Update list of CMS sites to test (comment if you do not want automatic updates)
$SAME_CRON/makesitelist.pl $SAME_DIR/client/etc/same.conf.CMS $SAME_DIR/client/etc/same.conf $SAME_WORKDIR 2>&1
if [ $? -ne 0 ] ; then
  /bin/echo "--- WARNING: could not update $SAME_DIR/client/etc/same.conf"
fi

export SAME_HOME=$SAME_DIR/client

# Use test lists for the production role
/bin/cp -f $SAME_DIR/client/sensors/CE/test-sequence-prod.lst $SAME_DIR/client/sensors/CE/test-sequence.lst 2>&1
/bin/cp -f $SAME_DIR/client/sensors/CREAMCE/test-sequence-prod.lst $SAME_DIR/client/sensors/CREAMCE/test-sequence.lst 2>&1
/bin/cp -f $SAME_DIR/client/sensors/testjob/test-sequence-prod.lst $SAME_DIR/client/sensors/testjob/test-sequence.lst 2>&1
/bin/cp -f $SAME_DIR/client/sensors/cream-testjob/test-sequence-prod.lst $SAME_DIR/client/sensors/cream-testjob/test-sequence.lst 2>&1

# Use configurations for production role
/bin/cp -f $SAME_DIR/client/sensors/CE/config-prod-wms.sh $SAME_DIR/client/sensors/CE/config-prod.sh 2>&1
/bin/cp -f $SAME_DIR/client/sensors/CE/config-prod.sh $SAME_DIR/client/sensors/CE/config.sh 2>&1
/bin/cp -f $SAME_DIR/client/sensors/CREAMCE/config-prod.sh $SAME_DIR/client/sensors/CREAMCE/config.sh 2>&1

/bin/echo ""
/bin/echo ""
/bin/echo -n "----------------------[ "
/bin/date

# Define the location of your certificate
export X509_USER_CERT=/afs/cern.ch/user/s/samcms/.globus-sciaba/usercert.pem
export X509_USER_KEY=/afs/cern.ch/user/s/samcms/.globus-sciaba/userkey.pem
export X509_USER_PROXY=$TMPFILE

# Choose an FQAN and a lifetime for the VOMS extensions
fqan='/cms/Role=production'
lifetime='12:00'

echo "+++ Using the $fqan FQAN"

voms-proxy-destroy
voms-proxy-init -debug -valid $lifetime -voms cms:$fqan -pwstdin < /afs/cern.ch/user/s/samcms/.pawlogin 2>&1
if [ $? != 0 ] ; then
  /bin/echo "--- FATAL: proxy creation failed!"
  /bin/rm -f $SAME_CRON/lock-ce-prod
  exit 1
fi  

for sensor in $publish_sensors ; do
  /bin/echo ""
  /bin/echo "----------"
  /bin/echo "Publishing $sensor sensor: "
  /bin/echo ""
  $SAME_DIR/client/bin/same-exec --publish $sensor 2>&1
done

for sensor in $sensors ; do
  /bin/echo ""
  /bin/echo "----------"
  /bin/echo "Executing $sensor sensor: "
  /bin/echo ""
  CONF=$SAME_DIR/client/etc/same.conf
  $SAME_DIR/client/bin/same-exec -c $CONF $sensor 2>&1
done

/bin/echo -n "ENDTIME: "
/bin/date "+%Y-%m-%d %H:%M:%S"

/bin/rm -f $TMPFILE
/bin/rm -f $SAME_CRON/lock-ce-prod
