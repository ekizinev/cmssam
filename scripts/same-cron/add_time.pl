#!/usr/bin/perl -w
#
# Script to add a timestamp line by line to a file
#
# Usage: use as a filter
#

while (<STDIN>) {
  my $date = `/bin/date "+%Y-%m-%d %H:%M:%S"`; chomp($date);
  print $date . ": " . $_;
}
