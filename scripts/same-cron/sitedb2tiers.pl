#!/usr/bin/perl -w

# Program to build site lists by tier number from SiteDB
#
#   Input: none
#   Writes the lists to files in the current directory
#

use LWP::Simple;
use XML::Parser;

# Small class for Sites

package Site;

sub new {
    my $class = shift;
    my $id = shift;
    my $self = {ID => $id, CMS => '', SAM => ''};
    bless $self, $class;
}

sub tier {
    my $self = shift;
    return substr $self->{CMS}, 1, 1;
}

# Main program

package main;

# Array with all Sites and tiers
%sites = ();
$tiers = [];

#Get XML file from SiteDB

my $url = "https://cmsweb.cern.ch/sitedb/sitedb/reports/showXMLReport/?reportid=cms_to_sam.ini";
my $doc = get($url) or die "Cannot retrieve XML\n";

# Parse XML

$p = new XML::Parser(Handlers => {Start => \&h_start, Char  => \&h_char});
$p->parse($doc) or die "Cannot parse XML\n";

# Exit if no sites are found

if (! %sites) {
    die "No sites found!\n";
}

# Classify by tier number

foreach my $s ( values %sites ) {
    my $t = $s->tier;
    next if ( $s->tier eq 'X' );
    push @{$tiers->[$t]}, $s->{SAM};
} 

# Write lists

for (my $t = 0; $t < @$tiers; $t++) {
    my $file = "CMS-T$t.txt";
    open (LIST, ">$file") or die "Cannot write site list\n";
    foreach (sort {uc($a) cmp uc($b)} @{$tiers->[$t]}) {
	printf LIST "%s\n", $_;
    }
    close LIST;
}

# Handler routines

sub h_start {
    my $p = shift;
    my $el = shift;
    my %attr = ();
    while (@_) {
	my $a = shift;
	my $v = shift;
	$attr{$a} = $v;
    }
    if ($el eq 'item') {
	$lastid = $attr{id};
	my $s = new Site($attr{id});
	$sites{$lastid} = $s;
    }
}

sub h_char {
    my $p = shift;
    my $a = shift;

    if ($p->in_element('cms_name')) {
	my $site = $sites{$lastid};
	$site->{CMS} = $a; 
    }
    if ($p->in_element('sam_name')) {
	my $site = $sites{$lastid};
	$site->{SAM} = $a; 
    }
}
